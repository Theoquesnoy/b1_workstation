# Maîtrise de poste - Day 1

## I. Self-footprinting
### Host OS

* Pour avoir les principales infos de la machine, il faut taper `systeminfo` :
```
PS C:\Users\theoq\workstation_b1\b1_workstation> systeminfo

Nom de l’hôte:                              LENOVO-THÉO
Nom du système d’exploitation:              Microsoft Windows 10 Famille
Version du système:                         10.0.18362 N/A version 18362
[...]
Modèle du système:                          81SY
Type du système:                            x64-based PC
[...]
Mémoire physique totale:                    8 112 Mo
Mémoire physique disponible:                3 948 Mo
Mémoire virtuelle : taille maximale:        15 024 Mo
Mémoire virtuelle : disponible:             8 405 Mo
Mémoire virtuelle : en cours d’utilisation: 6 619 Mo
[...]
```
* Concernant la RAM, la commande est `Get-WmiObject win32_physicalmemory | Format-Table Manufacturer,Banklabel,Configuredclockspeed,Devicelocator,Capacity,Serialnumber -autosize` : 

```
PS C:\Users\theoq> Get-WmiObject win32_physicalmemory | Format-Table Manufacturer,Banklabel,Configuredclockspeed,Devicelocator,Capacity,Serialnumber -autosize

Manufacturer Banklabel Configuredclockspeed Devicelocator    Capacity Serialnumber
------------ --------- -------------------- -------------    -------- ------------
SK Hynix     BANK 0                    2667 ChannelA-DIMM0 8589934592 2EDF0A22
```


### Devices

* Pour trouver la marque et le modèle du processeur, il faut marquer la commande `Get-WmiObject Win32_Processor` : 
```
PS C:\Users\theoq\workstation_b1\b1_workstation> Get-WmiObject Win32_Processor


Caption           : Intel64 Family 6 Model 158 Stepping 10
DeviceID          : CPU0
Manufacturer      : GenuineIntel
MaxClockSpeed     : 2400
Name              : Intel(R) Core(TM) i5-9300H CPU @ 2.40GHz
SocketDesignation : U3E1
```
Le nom du processeur Intel(R) Core(TM) i5-9300H CPU @ 2.40GHz correspond à la marque (Intel(R) Core(TM)), au modèle (i5-9300H) où "i5" désigne la catégorie du processeur. Ensuite,  le 1er des 4 chiffres correspond à la génération du processeur, et les 3 chiffres qui suivent correspondent à la référence du processeur, plus le 1er des 3 chiffres est élevé, plus le processeur est haut de gamme. La lettre après les 4 chiffres désigne le suffixe de la production qui correspond à l'utilité du processeur en question.

* Ensuite pour identifier le nombre de processeurs et le nombre de coeur, il faut utiliser la commande `Get-WmiObject -Class Win32_Processor | Select-Object -Property Name, Number*` :
```
PS C:\Users\theoq\workstation_b1\b1_workstation> Get-WmiObject -Class Win32_Processor | Select-Object -Property Name, Number*

Name                                     NumberOfCores NumberOfEnabledCore NumberOfLogicalProcessors
----                                     ------------- ------------------- -------------------------
Intel(R) Core(TM) i5-9300H CPU @ 2.40GHz             4                   4                         8
```

* Pour trouver la marque du touchpad/trackpad, il faudra utiliser la commande`Get-Help Get-TouchPad`, cependant cette commande ne fonctionne pas :
```
PS C:\Users\theoq\workstation_b1\b1_workstation> Get-Help Get-TouchPad
Get-Help : Get-Help n’est pas parvenu à trouver Get-TouchPad dans un fichier d’aide dans cette session. Pour
télécharger les rubriques d’aide mises à jour, tapez: «Update-Help». Pour obtenir de l’aide en ligne, cherchez la
rubrique d’aide dans la bibliothèque TechNet, à l’adresse https://go.microsoft.com/fwlink/?LinkID=107116.
Au caractère Ligne:1 : 1
+ Get-Help Get-TouchPad
+ ~~~~~~~~~~~~~~~~~~~~~
    + CategoryInfo          : ResourceUnavailable: (:) [Get-Help], HelpNotFoundException
    + FullyQualifiedErrorId : HelpNotFound,Microsoft.PowerShell.Commands.GetHelpCommand
```

* Pour trouver la marque et le modèle de la carte graphique, il faut utiliser la commande `wmic path win32_VideoController get name` : 

```
PS C:\Users\theoq\workstation_b1\b1_workstation> wmic path win32_VideoController get name
Name
NVIDIA GeForce GTX 1650
```

Pour le disque dur :
* Modèle et marque 
Pour le modèle et la marque du disque dur, la commande à utiliser est ` wmic diskdrive get Model` :
```
PS C:\Users\theoq\workstation_b1\b1_workstation> wmic diskdrive get Model
Model
WDC PC SN730 SDBPNTY-512G-1101
```

* Différentes partitions du disque 

La commande a utiliser est `Get-Partition` :
```
PS C:\Users\theoq\workstation_b1\b1_workstation> Get-Partition


   DiskPath : \\?\scsi#disk&ven_nvme&prod_wdc_pc_sn730_sdb#5&6e4e22b&0&000000#{53f56307-b6bf-11d0-94f2-00a0c91efb8b}

PartitionNumber  DriveLetter Offset                                        Size Type
---------------  ----------- ------                                        ---- ----
1                            1048576                                     260 MB System
2                            273678336                                    16 MB Reserved
3                C           290455552                                475.69 GB Basic
4                            511061262336                               1000 MB Recovery
```
Chaque partition à une utilité spécifique. Pour la partition 1, elle est réservée pour tout les fichiers requis qui serviront au bon fonctionnement du système.
Pour la 2ème, c'est une partition active qui stockes les fichiers de démarrage de Windows 10
Pour la 3ème, elle est consacrée au disque dur C
Pour la 4ème, elle est gardée pour la récupération de données


* Déterminer système de fichier de chaque partition

Pour déterminer le système de fichier de chaque partition, il faudra utiliser la commande `Get-Volume` :
```
PS C:\Users\theoq\workstation_b1\b1_workstation> Get-Volume

DriveLetter FriendlyName FileSystemType DriveType HealthStatus OperationalStatus SizeRemaining      Size
----------- ------------ -------------- --------- ------------ ----------------- -------------      ----
            WINRE_DRV    NTFS           Fixed     Healthy      OK                     556.6 MB   1000 MB
C           Windows-SSD  NTFS           Fixed     Healthy      OK                    322.41 GB 475.69 GB
```

### Users 

* Liste utilisateurs

Pour afficher la liste complète des utilisateurs, il faut utiliser la commande `Get-LocalUser` :

```
PS C:\Users\theoq\workstation_b1\b1_workstation> Get-LocalUser

Name               Enabled Description
----               ------- -----------
Administrateur     False   Compte d’utilisateur d’administration
DefaultAccount     False   Compte utilisateur géré par le système.
Invité             False   Compte d’utilisateur invité
theoq              True
WDAGUtilityAccount False   Compte d’utilisateur géré et utilisé par le système pour les scénarios Windows Defender A...
```
* Utilisateur admin sur la machine

Pour voir qui sont administrateurs sur une machine, il faut utiliser la commande `Get-LocalGroupMember -Group "Administrateurs"` :
```
PS C:\Users\theoq\workstation_b1\b1_workstation> Get-LocalGroupMember -Group "Administrateurs"

ObjectClass Name                       PrincipalSource
----------- ----                       ---------------
Utilisateur LENOVO-THÉO\Administrateur Local
Utilisateur LENOVO-THÉO\theoq          MicrosoftAccount
```

### Processus

* Liste des processus de la machine

Pour obtenir la liste des proccesus de la machine, il faut écrire `Get-Process` : 

```
PS C:\Users\theoq> Get-Process

Handles  NPM(K)    PM(K)      WS(K)     CPU(s)     Id  SI ProcessName
-------  ------    -----      -----     ------     --  -- -----------
    516      30    23272      47280       3,92  12972   1 ApplicationFrameHost
    607      40   238916     114076      35,64   2764   1 chrome
    249      16     6784      15480       0,19   2868   1 chrome
    249      16    16840      34460       0,16   3112   1 chrome
    400      25    98344     133092      43,55   3596   1 chrome
    279      22    63848      84596      10,61   4244   1 chrome
    256      16    19676      46448       0,42   5052   1 chrome
    234      16    16632      37192       0,27   5104   1 chrome
    487      30    27360      44468      27,09   8036   1 chrome
    216      13     7160      14952       0,39  10220   1 chrome
   1721      61    86052     143332      66,77  10680   1 chrome
    347      23    54228      96824      47,30  12204   1 chrome
    230      15    17536      37756       0,17  12596   1 chrome
    236      15    18088      36116       0,52  12824   1 chrome
    238       8     1808       7964       0,03  13228   1 chrome
    233      16    16064      35784       0,30  13420   1 chrome
    386      35   217212     215648     143,63  14924   1 chrome
    217      14    12144      21584       0,06  15768   1 chrome
    284      18    30552      53232       1,13  16272   1 chrome
    351      22    47820      78264       2,17  16992   1 chrome
    240      16    16344      32980       0,17  17060   1 chrome
    745      37    31908     141356      36,52   1752   1 Code
    259      16     9888      80472       0,16   2812   1 Code
    403      24    38832     122860       3,33   3092   1 Code
    487      33   145952     200264     133,27   5796   1 Code
    562      37   172896     167128      53,36   5856   1 Code
    437      21    13272     114244       0,50   6452   1 Code
    328      20    72908     119484      10,14   6888   1 Code
    266      16    11360      12612       0,09   3452   1 CodeHelper
    118       7     6428       5624              3276   0 conhost
    118       8     6520       6024       0,06   5668   1 conhost
    121       8     6516       1252       0,16   8500   1 conhost
    105       8     1452       6256       0,67   9332   1 conhost
    260      13     4304      15692       1,88  14300   1 conhost
    731      23     1876       4956               752   0 csrss
    868      29     2432       5488               852   1 csrss
    461      16     4304      13676       6,06   7200   1 ctfmon
[...]
```

Voici les 5 processus que j'ai choisi :
* Windows Defender SmartScreen :

Ce processus est une technologie de sécurité et de protection de Microsoft. Elle est incluse en natif sur Windows 7, 8 et 10. Elle aide à protéger le PC contre du contenu malveillants et des attaques WEB.

* Smss.exe :

Il s'agit du sous-système de gestion de session (Session Manager Subsystem).
Il est responsable du démarrage de la session utilisateur.
Ce processus est responsable de différentes activités dont le lancement des processus Winlogon et Win32 (csrss.exe, et qui est l'une des plus importantes) et du positionnement des variables système.
Après qu'il ait lancé ces processus, il attend que Winlogon ou Csrss se termine. Si cela se produit normalement, le système s'arrête.
C'est donc un processus essentiel car il est responsable d'une partie des activités dont , celui de csrss.exe
* csrss.exe :

Csrss signifie Client Server Run-time Subsystem.
C'est un sous-système essentiel qui doit fonctionner en permanence. Csrss gère les applications consoles, la création et la destruction de threads et quelques parties de l'environnement 16 bits virtuel MS-DOS

* svchost.exe :

svchost.exe est un des composants essentiels de Windows. Son rôle principal consiste à charger des bibliothèques de liens dynamique pour founir des fonctions à des applications quand elles en ont besoin. 
* Antimalware Service Executable :

C'est un antivirus qui constitue un processus fondamental du Windows Defender. Comme il s'agit d'un processus en arrière-plan, il fonctionne 24 heures sur 24 et 7 jours sur 7 pour vérifier la présence de logiciels Malware et de logiciels espions. C'est un processus fondamental qui aide au bon fonctionnement de Windows Defender qui protège le PC.

* Processus lancés par le full admin : 




### Network 

* On peut retrouver les infos concernant les cartes réseaux en réutilisant la commande `systeminfo` :

```
PS C:\Users\theoq> systeminfo

[...]
Carte(s) réseau:                            3 carte(s) réseau installée(s).
                                            [01]: Intel(R) Wireless-AC 9560 160MHz
                                                  Nom de la connexion : Wi-Fi
                                                  DHCP activé :         Oui
                                                  Serveur DHCP :        10.33.19.254
                                                  Adresse(s) IP
                                                  [01]: 10.33.19.64
                                                  [02]: fe80::b1ae:d610:c4fe:a451
                                            [02]: Realtek PCIe GbE Family Controller
                                                  Nom de la connexion : Ethernet
                                                  État :                Support déconnecté
                                            [03]: Bluetooth Device (Personal Area Network)
                                                  Nom de la connexion : Connexion réseau Bluetooth
                                                  État :                Support déconnecté
[...]
```

La carte réseau WI-FI sert à préparer, envoyer et de controler les données sur le réseau. Elle permet aussi de se connecter à Internet sans avoir besoin de câbles ou de fils.
Elles communiquent entre elles avec des ondes radios, basés sur la bande de fréquence 2.4GHz.

La carte réseau Ethernet prépare le câble réseau (Ethernet), les données émises par l'ordinateur, pour ensuite les transférer vers un autre ordianteur et ainsi contrôler le flux de données entre l'ordinateur et le câble. 

La carte réseau Bluetooth permet de communiquer avec des appareil électroniques qui possèdent eux aussi le bluetooth sur une courte distance. Elles échangent entre elles en utilisants des ondes radios sur la bande de fréquence de 2.4GHz, qui est la même que celle du WI-FI.

* Les ports TCP et UDP

Voici la liste de tout les ports TCP grâce à la commande `Get-NetTCPConnection` : 
```
PS C:\Users\theoq> Get-NetTCPConnection

LocalAddress                        LocalPort RemoteAddress                       RemotePort State       AppliedSetting
------------                        --------- -------------                       ---------- -----       --------------
::                                  54288     ::                                  0          Listen
::                                  49670     ::                                  0          Listen
::1                                 49669     ::                                  0          Listen
::                                  49668     ::                                  0          Listen
::                                  49667     ::                                  0          Listen
::                                  49666     ::                                  0          Listen
::                                  49665     ::                                  0          Listen
::                                  49664     ::                                  0          Listen
::                                  7680      ::                                  0          Listen
::                                  5357      ::                                  0          Listen
::                                  445       ::                                  0          Listen
::                                  135       ::                                  0          Listen
0.0.0.0                             63979     0.0.0.0                             0          Bound
0.0.0.0                             63978     0.0.0.0                             0          Bound
0.0.0.0                             63977     0.0.0.0                             0          Bound
0.0.0.0                             63976     0.0.0.0                             0          Bound
0.0.0.0                             63975     0.0.0.0                             0          Bound
0.0.0.0                             63974     0.0.0.0                             0          Bound
0.0.0.0                             63973     0.0.0.0                             0          Bound
0.0.0.0                             63972     0.0.0.0                             0          Bound
0.0.0.0                             63971     0.0.0.0                             0          Bound
0.0.0.0                             63970     0.0.0.0                             0          Bound
0.0.0.0                             63964     0.0.0.0                             0          Bound
0.0.0.0                             63963     0.0.0.0                             0          Bound
0.0.0.0                             63961     0.0.0.0                             0          Bound
0.0.0.0                             63959     0.0.0.0                             0          Bound
0.0.0.0                             63957     0.0.0.0                             0          Bound
0.0.0.0                             63955     0.0.0.0                             0          Bound
0.0.0.0                             63948     0.0.0.0                             0          Bound
0.0.0.0                             63879     0.0.0.0                             0          Bound
0.0.0.0                             63878     0.0.0.0                             0          Bound
0.0.0.0                             63872     0.0.0.0                             0          Bound
0.0.0.0                             63864     0.0.0.0                             0          Bound
0.0.0.0                             63843     0.0.0.0                             0          Bound
0.0.0.0                             63826     0.0.0.0                             0          Bound
0.0.0.0                             63822     0.0.0.0                             0          Bound
0.0.0.0                             63807     0.0.0.0                             0          Bound
0.0.0.0                             63795     0.0.0.0                             0          Bound
0.0.0.0                             63794     0.0.0.0                             0          Bound
0.0.0.0                             63788     0.0.0.0                             0          Bound
0.0.0.0                             63758     0.0.0.0                             0          Bound
0.0.0.0                             63753     0.0.0.0                             0          Bound
0.0.0.0                             63752     0.0.0.0                             0          Bound
0.0.0.0                             63703     0.0.0.0                             0          Bound
0.0.0.0                             63702     0.0.0.0                             0          Bound
0.0.0.0                             63701     0.0.0.0                             0          Bound
0.0.0.0                             63699     0.0.0.0                             0          Bound
0.0.0.0                             63696     0.0.0.0                             0          Bound
0.0.0.0                             63690     0.0.0.0                             0          Bound
0.0.0.0                             63688     0.0.0.0                             0          Bound
0.0.0.0                             63391     0.0.0.0                             0          Bound
0.0.0.0                             63390     0.0.0.0                             0          Bound
0.0.0.0                             63372     0.0.0.0                             0          Bound
0.0.0.0                             63370     0.0.0.0                             0          Bound
0.0.0.0                             63258     0.0.0.0                             0          Bound
0.0.0.0                             63078     0.0.0.0                             0          Bound
0.0.0.0                             63041     0.0.0.0                             0          Bound
0.0.0.0                             63035     0.0.0.0                             0          Bound
0.0.0.0                             49757     0.0.0.0                             0          Bound
0.0.0.0                             49738     0.0.0.0                             0          Bound
0.0.0.0                             49737     0.0.0.0                             0          Bound
127.0.0.1                           65001     127.0.0.1                           63035      Established Internet
127.0.0.1                           65001     0.0.0.0                             0          Listen
192.168.43.87                       63979     51.138.106.75                       443        Established Internet
192.168.43.87                       63978     216.58.204.110                      443        Established Internet
192.168.43.87                       63977     216.58.204.110                      443        Established Internet
192.168.43.87                       63976     216.58.214.74                       443        Established Internet
[...]
```

Et voici la liste des ports UDP avec la commande `Get-NetUDPEndpoint` : 
```
PS C:\Users\theoq> Get-NetUDPEndpoint

LocalAddress                             LocalPort
------------                             ---------
::                                       59627
::1                                      54345
fe80::b1ae:d610:c4fe:a451%4              54344
::                                       54184
::                                       53558
::                                       5355
::1                                      5353
::                                       5353
::                                       4500
::                                       3702
fe80::b1ae:d610:c4fe:a451%4              1900
::1                                      1900
::                                       500
::                                       123
0.0.0.0                                  62995
0.0.0.0                                  59626
127.0.0.1                                57330
127.0.0.1                                54347
192.168.43.87                            54346
0.0.0.0                                  54183
0.0.0.0                                  53557
127.0.0.1                                49664
0.0.0.0                                  27036
127.0.0.1                                10011
127.0.0.1                                10010
0.0.0.0                                  5355
192.168.43.87                            5353
0.0.0.0                                  5353
0.0.0.0                                  5353
0.0.0.0                                  5050
0.0.0.0                                  4500
0.0.0.0                                  3702
192.168.43.87                            1900
127.0.0.1                                1900
0.0.0.0                                  500
192.168.43.87                            138
192.168.43.87                            137
0.0.0.0                                  123
```

Pour déterminer quel programmes tourne derrière chacun des ports TCP, on utilise la commande `netstat -a -b -n -o -p tcp` :
```
PS C:\Windows\system32> netstat -a -b -n -o -p tcp

Connexions actives

  Proto  Adresse locale         Adresse distante       État
  TCP    0.0.0.0:135            0.0.0.0:0              LISTENING       1196
  RpcSs
 [svchost.exe]
  TCP    0.0.0.0:445            0.0.0.0:0              LISTENING       4
 Impossible d’obtenir les informations de propriétaire
  TCP    0.0.0.0:5040           0.0.0.0:0              LISTENING       7984
  CDPSvc
 [svchost.exe]
  TCP    0.0.0.0:5357           0.0.0.0:0              LISTENING       4
 Impossible d’obtenir les informations de propriétaire
  TCP    0.0.0.0:27036          0.0.0.0:0              LISTENING       6284
 [Steam.exe]
  TCP    0.0.0.0:49664          0.0.0.0:0              LISTENING       948
 [lsass.exe]
  TCP    0.0.0.0:49665          0.0.0.0:0              LISTENING       856
 Impossible d’obtenir les informations de propriétaire
  TCP    0.0.0.0:49666          0.0.0.0:0              LISTENING       924
  EventLog
 [svchost.exe]
  TCP    0.0.0.0:49667          0.0.0.0:0              LISTENING       2712
  Schedule
 [svchost.exe]
  TCP    0.0.0.0:49668          0.0.0.0:0              LISTENING       3848
 [spoolsv.exe]
  TCP    0.0.0.0:49673          0.0.0.0:0              LISTENING       928
 Impossible d’obtenir les informations de propriétaire
  TCP    10.33.2.55:139         0.0.0.0:0              LISTENING       4
 Impossible d’obtenir les informations de propriétaire
  TCP    10.33.2.55:50391       40.67.254.36:443       ESTABLISHED     4204
  WpnService
 [svchost.exe]
  TCP    10.33.2.55:50625       173.194.76.188:5228    ESTABLISHED     284
 [chrome.exe]
  TCP    10.33.2.55:50646       18.179.138.200:443     ESTABLISHED     284
 [chrome.exe]
  TCP    10.33.2.55:50688       162.159.130.234:443    ESTABLISHED     2804
 [Discord.exe]
  TCP    10.33.2.55:50689       84.53.156.184:80       TIME_WAIT       0
  TCP    10.33.2.55:50691       20.44.232.74:443       ESTABLISHED     7532
  CDPUserSvc_78077
 [svchost.exe]
  TCP    10.33.2.55:50692       3.218.125.188:443      ESTABLISHED     284
 [chrome.exe]
  TCP    10.33.2.55:50693       3.218.125.188:443      ESTABLISHED     284
 [chrome.exe]
  TCP    127.0.0.1:6463         0.0.0.0:0              LISTENING       4696
 [Discord.exe]
  TCP    127.0.0.1:27060        0.0.0.0:0              LISTENING       6284
 [Steam.exe]
  TCP    127.0.0.1:49712        0.0.0.0:0              LISTENING       10100
 [NVIDIA Web Helper.exe]
  TCP    127.0.0.1:49712        127.0.0.1:50361        ESTABLISHED     10100
 [NVIDIA Web Helper.exe]
  TCP    127.0.0.1:50355        127.0.0.1:65001        ESTABLISHED     4228
 [nvcontainer.exe]
  TCP    127.0.0.1:50361        127.0.0.1:49712        ESTABLISHED     10664
 [NVIDIA Share.exe]
  TCP    127.0.0.1:65001        0.0.0.0:0              LISTENING       4228
 [nvcontainer.exe]
  TCP    127.0.0.1:65001        127.0.0.1:50355        ESTABLISHED     4228
 [nvcontainer.exe]
```

Et pour les ports UDP, on utilise la commande `netstat -a -b -n -o -p udp` :

```
PS C:\Windows\system32> netstat -a -b -n -o -p udp

Connexions actives

  Proto  Adresse locale         Adresse distante       État
  UDP    0.0.0.0:500            *:*                                    4148
  IKEEXT
 [svchost.exe]
  UDP    0.0.0.0:3702           *:*                                    6844
  FDResPub
 [svchost.exe]
  UDP    0.0.0.0:3702           *:*                                    6844
  FDResPub
 [svchost.exe]
  UDP    0.0.0.0:3702           *:*                                    2388
 [dashost.exe]
  UDP    0.0.0.0:3702           *:*                                    2388
 [dashost.exe]
  UDP    0.0.0.0:4500           *:*                                    4148
  IKEEXT
 [svchost.exe]
  UDP    0.0.0.0:5050           *:*                                    7984
  CDPSvc
 [svchost.exe]
  UDP    0.0.0.0:5353           *:*                                    2176
 [chrome.exe]
  UDP    0.0.0.0:5353           *:*                                    2176
 [chrome.exe]
  UDP    0.0.0.0:5353           *:*                                    2984
  Dnscache
 [svchost.exe]
  UDP    0.0.0.0:5355           *:*                                    2984
  Dnscache
 [svchost.exe]
  UDP    0.0.0.0:27036          *:*                                    6284
 [Steam.exe]
  UDP    0.0.0.0:49603          *:*                                    4228
 [nvcontainer.exe]
  UDP    0.0.0.0:58439          *:*                                    6284
 [Steam.exe]
  UDP    0.0.0.0:59695          *:*                                    6844
  FDResPub
 [svchost.exe]
  UDP    0.0.0.0:61460          *:*                                    2388
 [dashost.exe]
  UDP    10.33.2.55:137         *:*                                    4
 Impossible d’obtenir les informations de propriétaire
  UDP    10.33.2.55:138         *:*                                    4
 Impossible d’obtenir les informations de propriétaire
  UDP    10.33.2.55:1900        *:*                                    5568
  SSDPSRV
 [svchost.exe]
  UDP    10.33.2.55:5353        *:*                                    4228
 [nvcontainer.exe]
  UDP    10.33.2.55:60367       *:*                                    5568
  SSDPSRV
 [svchost.exe]
  UDP    127.0.0.1:1900         *:*                                    5568
  SSDPSRV
 [svchost.exe]
  UDP    127.0.0.1:10010        *:*                                    10100
 [NVIDIA Web Helper.exe]
  UDP    127.0.0.1:10011        *:*                                    7456
 [nvcontainer.exe]
  UDP    127.0.0.1:49666        *:*                                    4636
  iphlpsvc
 [svchost.exe]
  UDP    127.0.0.1:56318        *:*                                    2684
  NlaSvc
 [svchost.exe]
  UDP    127.0.0.1:58121        *:*                                    7508
 [nvcontainer.exe]
  UDP    127.0.0.1:60368        *:*                                    5568
  SSDPSRV
 [svchost.exe]
```

Pour faire correspondre l'id d'un proccesus à un programme, il faut regarder la dernière colonne, qui correspond au PID (Identificateur De Processus) : 

* PID 0 = Proccesus inactifs du système :

Ce sont tout les processus qui sont arretés ou mis en pause par le système.

* PID 4 = System :

La plupart des threads du mode noyau fonctionnent en tant que processus System. Il utilise les ports TCP et UDP.

* PID 948 = lsass.exe C'est un exécutable qui est nécessaire pour le bon fonctionnement de Windows. Il assure l'identification des utilisateurs.
* PID 2388 = DasHost.exe Il exécute l'hôte de fournisseur Device Association Framework Framework qui connecte et associe les périphériques câblés et sans fil avec le système d'exploitation Windows. Il s'agit d'un fichier Windows essentiel. Il ne doit pas être supprimé sauf si la cause de problèmes est connue.
* PID 2684,5568, 2717... = svchost.exe (explication du processus plus haut) Ce processus utilise les ports TCP et UDP.
* PID 284 = chrome.exe Il utilise les ports TCP et UDP. Ce processus est l'executable du navigateur Google Chrome.


### II. Scripting

voir TP1-script1.ps1 et TP1-script2.ps1 dans le repo git

### III. Gestionnaire de softs

Un gestionnaire de parquets est un système qui permet d'installer des logiciels, de les maintenir à jour, et de les désinstaller.
* Concernant les téléchargements sur internet, le gestionnaire de softs va directement chercher le stockage du ficher à télécharger et va les installer automatiquement.
* Pour les identités impliquées, il y a nous, internet (pour aller sur le serveur du fichier à télécharger), le serveur de l'éditeur, et un site tiers qui assure la protection et la fiabilité du site (https avec le cadenas vert).
* Pour la sécurité, il y a plusieurs critères qui rentrent comme l'antivirus/l'antimalware qui va analyser le fichier téléchargé; le pare-feu qui va autoriser ou non certains accès, etc.

Liste des paquets installés :
```
PS C:\Windows\system32> choco list --local-only
Chocolatey v0.10.15
chocolatey 0.10.15
1 packages installed.
```
Provenance des paquets :
```
PS C:\Windows\system32> choco source
Chocolatey v0.10.15
chocolatey - https://chocolatey.org/api/v2/ | Priority 0|Bypass Proxy - False|Self-Service - False|Admin Only - False.
```

### IV. Machine virtuel

Voici le résultat du partage de fichiers accessible dans la VM :
* J'ai d'abord rendu le dossier à partager visible dans la VM, avec la preuve qu'il est bien possible d'y accéder :
```
[root@localhost ~]# mount -t cifs -o username=toto,password=123456 //192.168.120.1/PCTheoSMB /mnt/test
[root@localhost ~]# df -h
Filesystem                 Size  Used Avail Use% Mounted on
devtmpfs                   484M     0  484M   0% /dev
tmpfs                      496M     0  496M   0% /dev/shm
tmpfs                      496M  6.8M  489M   2% /run
tmpfs                      496M     0  496M   0% /sys/fs/cgroup
/dev/mapper/centos-root    6.2G  1.3G  5.0G  21% /
/dev/sda1                 1014M  137M  878M  14% /boot
tmpfs                      100M     0  100M   0% /run/user/1000
tmpfs                      100M     0  100M   0% /run/user/0
//192.168.120.1/PCTheoSMB  476G  158G  318G  34% /mnt/test
```

* J'ai donc ensuite créer un fichier pour voir s'il était possible de créer, modifer, supprimer un fichier et cela est effectivement possible :
```
[root@localhost ~]# cd /mnt/test
[root@localhost test]# touch toto
[root@localhost test]# ls
text.txt  toto
```