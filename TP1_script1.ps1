# Théo QUESNOY
# 19/10/2020
# Afficher un résumé de l'OS, élément par élement chacun sur sa ligne

Write-Output "Nom de l'ordi : $env:computername"

$ip = (Test-Connection -ComputerName $env:computername -count 1).IPV4Address.ipaddressTOstring
Write-Output "Ip principale : $ip"

$OS = (Get-WMIObject win32_operatingsystem).name
$OS = $OS.split("|")[0]
Write-Output "OS : $OS"

$OSVersion = (Get-WMIObject win32_operatingsystem).version
Write-Output "OS Version : $OSVersion"

$DateEtHeure = (Get-CimInstance Win32_OperatingSystem).LastBootUpTime 
Write-Output "Date et heure d'allumage : $DateEtHeure"

$Option = "Type='software' and IsAssigned=1 and IsHidden=0 and IsInstalled=0"
$Recherche = (New-Object -COM Microsoft.Update.Session).CreateUpdateSearcher()
$Updates = $Recherche.Search($Option).Updates

if ($Updates.Count -ne 0) {
    $OSupdated = "true"
}
else {
    $OSupdated = "false"
}
Write-Output "Is OS up-to-date : $OSupdated"
Write-Output " "

$ping = (Test-Connection -ComputerName "8.8.8.8" -Count 4  | measure-Object -Property ResponseTime -Average).average
Write-Output "average ping time : $ping ms"
$downloadspeed = [math]::Round($SpeedtestResults.download.bandwidth / 1000000 * 8, 2)
$uploadspeed = [math]::Round($SpeedtestResults.upload.bandwidth / 1000000 * 8, 2)
Write-Output "average download speed : $downloadSpeed Mbit/s"
Write-Output "average upload speed : $uploadspeed Mbit/s"
Write-Output " "

Write-Output "RAM"
Write-Output " "
$RAMdispo = (Get-CIMInstance Win32_OperatingSystem).FreePhysicalMemory
Write-Output "RAM Free : $RAMdispo i"
$RAMutilise = [String]((Get-WmiObject -Class Win32_OperatingSystem).FreePhysicalMemory / 1MB)
Write-Output "RAM Used : $RAMutilise i"
Write-Output " "

Write-Output "Disk"
Write-Output " "
$DisqueFree = (get-WmiObject win32_logicaldisk).FreeSpace
Write-Output "Free Space : $DisqueFree G"
$DisqueUtilise = [String]((get-WmiObject win32_logicaldisk).FreeSpace / 1MB)
Write-Output "Disk Used : $DisqueUtilise G"
Write-Output " "

$Users = (Get-WmiObject Win32_UserAccount).Name 
Write-Output "Users list : $Users"